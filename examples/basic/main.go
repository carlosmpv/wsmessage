package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"codeberg.org/carlosmpv/wsmessage"
	"codeberg.org/carlosmpv/wsmessage/authentication"
	"github.com/golang-jwt/jwt/v5"
)

type ClientClaims struct {
	Username string `json:"username"`
	jwt.RegisteredClaims
}

func main() {
	j := authentication.New()

	layer0 := func(ctx context.Context, message wsmessage.Message) (wsmessage.Message, wsmessage.MachineTransition) {
		claims, ok := ctx.Value(authentication.MiddlewareContextKey("claims")).(*ClientClaims)
		if !ok {
			panic("could not cast to ClientClaims")
		}

		log.Println("claims", claims)

		switch message.Type {
		case wsmessage.Error:
			// s.error(message, output)
			return wsmessage.Message{}, nil

		default:

			return wsmessage.ErrorMessage(fmt.Errorf("unexpected message type: %s", message.Type)), nil
		}

	}

	protectedLayer0 := authentication.Middleware(j.VerifyTokenMiddleware(&ClientClaims{})).Apply(layer0)

	init := j.LoginHandler(func(ctx context.Context, m wsmessage.Message) (jwt.Claims, error) {
		return &ClientClaims{
			Username: "Carlosmpv",
			RegisteredClaims: jwt.RegisteredClaims{
				ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
			},
		}, nil
	}, protectedLayer0)

	service := wsmessage.New(init)
	log.Fatal(http.ListenAndServe(":8080", service))
}
