module codeberg.org/carlosmpv/wsmessage

go 1.21.5

require (
	github.com/golang-jwt/jwt/v5 v5.2.0 // indirect
	github.com/rs/xid v1.5.0 // indirect
	golang.org/x/net v0.20.0 // indirect
)
