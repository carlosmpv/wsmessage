package wsmessage

import (
	"context"
	"io"
	"log"
	"net/http"
	"net/url"

	"golang.org/x/net/websocket"
)

type MessageType string

const (
	Error                MessageType = "error"
	LoginRequest         MessageType = "login"
	LoginSuccessResponse MessageType = "login-success"
)

type Message struct {
	Token string      `json:"token"`
	Type  MessageType `json:"type"`
	Raw   []byte      `json:"raw"`
}

func ErrorMessage(err error) Message {
	return Message{
		Token: "",
		Type:  Error,
		Raw:   []byte(err.Error()),
	}
}

func New(mt MachineTransition) *Service {
	return &Service{
		machine: mt,
	}
}

type Service struct {
	machine MachineTransition
}

type MachineTransition func(context.Context, Message) (Message, MachineTransition)

func (s *Service) wsHandler(ws *websocket.Conn) {
	origin, err := url.Parse("*")
	if err != nil {
		log.Fatal(err)
	}

	defer ws.Close()
	ws.Config().Origin = origin
	// Esta rota inicia conexões websocket
	// 1. Armazena uma identificação do usuário
	// 2. Associa essa identificação para um canal de envio de mensagens
	// 3. Mensagens são recebidas em paralelo

	// output is the end of the pipeline, which will be used to send messages to the user
	output := make(chan Message)
	lifetime, die := context.WithCancel(context.Background())
	state := s.machine

	go func() {
	loop:
		for {
			select {
			case <-lifetime.Done():
				break loop
			case toBeSent := <-output:
				if err := websocket.JSON.Send(ws, toBeSent); err != nil {
					log.Println(err.Error())
					die()
					break loop
				}
			}
		}
	}()

	go func() {
	loop:
		for {

			select {
			case <-lifetime.Done():
				break loop
			default:
				var message Message
				if err := websocket.JSON.Receive(ws, &message); err != nil {
					// handle error
					if err != io.EOF {
						log.Println(err.Error())
					}

					die()
					break loop
				}

				if state == nil {
					die()
					break loop
				}

				var response Message
				response, state = state(lifetime, message)
				output <- response

				// handle received message
				// go s.work(message, output)
			}
		}
	}()

	<-lifetime.Done()
}

func (s *Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	websocket.Server{Handler: s.wsHandler}.ServeHTTP(w, r)
}
