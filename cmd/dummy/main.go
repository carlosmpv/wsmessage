package main

import (
	"errors"
	"log"
	"time"

	"codeberg.org/carlosmpv/wsmessage"
	"golang.org/x/net/websocket"
)

type Step struct {
	Message  *wsmessage.Message
	NextStep func(wsmessage.Message) (*Step, error)
}

func main() {
	ws, err := websocket.Dial("ws://localhost:8080/ws", "", "http://localhost/")
	if err != nil {
		log.Fatal(err)
	}

	defer ws.Close()

	step := &Step{
		Message: &wsmessage.Message{
			Token: "",
			Type:  wsmessage.LoginRequest,
			Raw:   []byte("carlosmpv @4EachOne"),
		},
		NextStep: func(m wsmessage.Message) (*Step, error) {
			if m.Type == wsmessage.Error {
				return nil, errors.New(string(m.Raw))
			}

			return &Step{
				Message: &wsmessage.Message{
					Token: string(m.Raw),
					Type:  wsmessage.LoginRequest,
					Raw:   []byte("carlosmpv @4EachOne"),
				},
				NextStep: func(m wsmessage.Message) (*Step, error) {
					if m.Type == wsmessage.Error {
						return nil, errors.New(string(m.Raw))
					}

					return nil, nil
				},
			}, nil
		},
	}

	for {
		time.Sleep(200 * time.Millisecond)
		if err := websocket.JSON.Send(ws, &step.Message); err != nil {
			log.Println(err)
		}

		var message wsmessage.Message
		if err := websocket.JSON.Receive(ws, &message); err != nil {
			log.Println(err)
		}

		log.Println("received: ", message)
		if next, err := step.NextStep(message); err == nil {
			if next == nil {
				log.Println("plan has ended")
				break
			}

			step = next
		} else {
			log.Fatal(err)
			break
		}
	}

}
