package authentication

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"codeberg.org/carlosmpv/wsmessage"
	"github.com/golang-jwt/jwt/v5"
)

func generateRandomKeyBase64(length int) string {
	key := make([]byte, length)
	_, err := rand.Read(key)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(key)
}

func New(secretKey ...string) *JwtHelper {
	if len(secretKey) == 0 {
		return New(generateRandomKeyBase64(10))
	}

	return &JwtHelper{
		secretKey: []byte(secretKey[0]),
	}
}

type JwtHelper struct {
	secretKey []byte
}

func (jh JwtHelper) CreateToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jh.secretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (jh JwtHelper) verifyToken(claims jwt.Claims, tokenString string) (jwt.Claims, error) {
	token, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) {
		return jh.secretKey, nil
	})

	if err != nil {
		return claims, err
	}

	if !token.Valid {
		return claims, fmt.Errorf("invalid token")
	}

	return claims, nil
}

func (jh JwtHelper) LoginHandler(login func(context.Context, wsmessage.Message) (jwt.Claims, error), next wsmessage.MachineTransition) wsmessage.MachineTransition {
	return wsmessage.MachineTransition(func(ctx context.Context, msg wsmessage.Message) (wsmessage.Message, wsmessage.MachineTransition) {
		claims, err := login(ctx, msg)
		if err != nil {
			return wsmessage.ErrorMessage(err), nil
		}

		if token, err := jh.CreateToken(claims); err != nil {
			return wsmessage.ErrorMessage(err), nil
		} else {
			return wsmessage.Message{
				Token: "",
				Type:  wsmessage.LoginSuccessResponse,
				Raw:   []byte(token),
			}, next
		}
	})
}

func (jh JwtHelper) VerifyTokenMiddleware(claims jwt.Claims) func(ctx context.Context, message wsmessage.Message) (context.Context, error) {
	return func(ctx context.Context, message wsmessage.Message) (context.Context, error) {
		if client, err := jh.verifyToken(claims, message.Token); err != nil {
			return ctx, err
		} else {
			return context.WithValue(ctx, MiddlewareContextKey("claims"), client), nil
		}
	}
}

type Middleware func(ctx context.Context, message wsmessage.Message) (context.Context, error)

type MiddlewareContextKey string

func (m Middleware) Apply(transition wsmessage.MachineTransition, catch ...wsmessage.MachineTransition) wsmessage.MachineTransition {
	if len(catch) == 0 {
		return m.Apply(transition, nil)
	}

	return wsmessage.MachineTransition(func(ctx context.Context, message wsmessage.Message) (wsmessage.Message, wsmessage.MachineTransition) {
		if nCtx, err := m(ctx, message); err != nil {
			return wsmessage.ErrorMessage(err), catch[0]
		} else {
			return transition(nCtx, message)
		}
	})
}
